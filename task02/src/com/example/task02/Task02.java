package com.example.task02;

public class Task02 {

    public static String solution(String input) {

        // TODO напишите здесь свою корректную реализацию этого метода, вместо сеществующей

        long v = Long.parseLong(input);
        if (v >= Byte.MIN_VALUE && v <= Byte.MAX_VALUE) {
            return "byte";
        }

        if (v >= Short.MIN_VALUE && v <= Short.MAX_VALUE) {
            return "short";
        }

        if (v >= Integer.MIN_VALUE && v <= Integer.MAX_VALUE) {
            return "int";
        }

        if (v >= Long.MIN_VALUE && v <= Long.MAX_VALUE) {
            return "long";
        }

        return "";
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        String result = solution("12345");
        System.out.println(result);
         */
    }

}
